# SA-Practica10

Practica No. 10 de Software Avanzado.

## Descripcion
* Utilizar la práctica # 7 como base.
* Crear un repositorio nuevo (base DevOps) que sea capaz de utilizar el repositorio de dicha práctica para construir (si hace falta) y desplegar el ambiente completo (base de datos y aplicación) y que esta práctica 7 corra sobre Docker.
* Entrega en repositorio y video de demostración (2 minutos máximo).

## Requisitos
* Node js

## Librerias

```
npm i express
npm i fs
npm i body-parser
npm i axios
npm i chai
npm i chai-http
npm i mocha
npm i nyc --save-dev
```

## Ejecucion
```
node Cliente/index.js
node Restaurante/index.js
node Repartidor/index.js
node log/index.js
node ESB/orquestador.js
```

## Pruebas
```
ng test
```

## Artefactos
* [Artefacto Cliente](https://hub.docker.com/r/andreeavalos/cliente)
* [Artefacto Log](https://hub.docker.com/r/andreeavalos/log)
* [Artefacto Repartidor](https://hub.docker.com/r/andreeavalos/repartidor)
* [Artefacto Restaurante](https://hub.docker.com/r/andreeavalos/restaurante)
* [Artefacto ESB](https://hub.docker.com/r/andreeavalos/esb)

## Colaborador

* Carlos Andree Avalos Soto 201408580

## Videos Demostrativos
* [Practica 4-Explicacion de Orquestador](https://drive.google.com/file/d/1F3gX7wZ6hglRYXTitLQyNJZJpu-getno/view)
* [Practica 6-Coverage](https://drive.google.com/file/d/1_z8JqL3jlfsR0DwsPn6TQGeNfMrlcBLM/view)
* [Practica 7-Explicacion CI](https://drive.google.com/file/d/1O7ztR4a0yKM3vOREKopI5BERASSPnKM1/view)
* [Practica 10-Docker-Compose](https://drive.google.com/file/d/1dl6m2hKyYCt3XKpkp0Kdh3_eUM6nxh14/view)